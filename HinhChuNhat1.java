package BaitapBuoi3;

public class HinhChuNhat1 {
    //Khai báo thuộc tính
    double dai, rong;

    //Phương thức thiết lạp (set) và lấy (get) thông tin chiều dài chiều rộng

    public double getRong() {
        return rong;
    }

    public double getDai() {
        return dai;
    }

    public void setRong(double rong) {
        this.rong = rong;
    }

    public void setDai(double dai) {
        this.dai = dai;
    }

    //Phương thức tính diện tích
    public double tinhDienTich() {
        return dai * rong;
    }
    //Phương thức tính chu vi
    public double tinhChuVi() {

        return (dai + rong) * 2;
    }
    public String toString(){//ghi de phuong thuc toString()
        return "Chiều dài"+" " +dai+" "+"Chiều rộng" +" "+rong+" "+"Diện Tích" +" "+ tinhDienTich()+" "+"Chu Vi"+" "+ tinhChuVi() ;
    }
}

