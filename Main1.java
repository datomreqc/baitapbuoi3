package BaitapBuoi3;
import java.util.Scanner;
public class Main1 {
    static void nhapXe(Vehicle xe){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập dung tích xe: ");
        xe.setDungTich(scanner.nextInt());;scanner.nextLine();
        System.out.print("Nhập chủ xe: ");
        xe.setChuXe(scanner.nextLine());
        System.out.print("Nhập trị giá xe: ");
        xe.setTriGia(scanner.nextDouble());;scanner.nextLine();
        System.out.print("Nhập hãng xe: ");
        xe.setHangXe(scanner.nextLine());
        System.out.print("Nhập màu xe: ");
        xe.setMauXe(scanner.nextLine());
    }
    public static void main(String[] args) {
        //Bai1

        System.out.println("bài 1");
        HinhChuNhat1 hcn = new HinhChuNhat1();
        Scanner scanner = new Scanner(System.in);

        System.out.print("\nNhập vào chiều dài HCN : ");
        hcn.setDai(scanner.nextDouble());
        System.out.print("Nhập vào chiều rộng HCN : ");
        hcn.setRong(scanner.nextDouble());

        System.out.println(hcn);

        //Bai2:
        System.out.println("\nbài 2");

        SinhVien sv1 = new SinhVien(1001,"Tống Thị Nga",8,9);
        SinhVien sv2 = new SinhVien(1002,"Nguyễn Minh Tiến",9,9);
        SinhVien sv3 = new SinhVien();

        System.out.print("\nnhập vào Mã sinh viên : ");
        sv3.setMSV(scanner.nextInt());scanner.nextLine();;
        System.out.print("nhập vào họ tên sinh viên : ");
        sv3.setHoTen(scanner.nextLine());
        System.out.print("nhập vào điểm lý thuyết : ");
        sv3.setDiemLT((float) scanner.nextDouble());
        System.out.print("nhập vào điểm thực hành : ");
        sv3.setDiemTH((float) scanner.nextDouble());

        System.out.println("Danh Sách Sinh Viên: ");
        System.out.printf("%6s %12s %23s %16s %19s \n","Mã sinh viên","Họ tên","Điểm lý thuyết","Điểm thực hành","Điểm trung bình");
        sv1.inSV();
        sv2.inSV();
        sv3.inSV();

        //Bai3:
        System.out.println("\nbài3");;
        System.out.print("Nhập số lượng xe bạn muốn khai báo thuế: ");
        int n=scanner.nextInt();
        Vehicle[] v;
        v = new Vehicle[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Xe thứ " + (i + 1));
            v[i] = new Vehicle();
            nhapXe(v[i]);
        }
        System.out.println("Bảng kê khai tiền thuế trước bạ của các xe:");
        System.out.printf("%12s %13s %12s %15s %13s %15s\n","Tên chủ xe","Hãng xe","Dung tích","Giá trị","Màu","Tiền Thuế");
        for (int i = 0; i < n; i++) {
            v[i].inThue();
        }
        System.exit(1);

    }
}

